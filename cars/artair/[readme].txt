Car information
================================================================
Car name: Artair
Car Type: Remodel
Top speed: 66 kph
Rating/Class: 5 (Pro)
Installed folder: ...\cars\artair
Description: 

This is Artair, a remodel of Sakyû by Xarc.
The car has a very soft suspension, and a Pro-class rating.

Params by Mighty, remodel and texture by Kiwi.

Have fun!
Kiwi & Mighty Cucumber

Author Information
================================================================
Remodel: Kiwi
Texture: Kiwi
Parameters: MightyCucumber
 
Construction
================================================================
Base model: Sakyû (by Xarc)
Editor(s) used: Notepad++, Blender (with ReVolt-Plugin from Marv), PhotoImpact12
 
Copyright / Permissions
================================================================
You may do whatever you want with this car, as long as you mention us and the original author in the credits.

Version 1.1 from December 28th, 2018