=============================
made by BloodBTF, December 2019

Name: Cerberus
Class: Glow
Rating: Pro
Top Speed: ~41MPH
Mass: 1.85kg
Trans: 4WD
Accel: great
Handling: good

A valiant offroad racer well equiped to leave the competition in the dust, if you can learn to tame it. Optimized suspension makes it fly nice and level in the air, while having good handling on the ground. This truck has plenty of grip, which can be both a blessing and curse, since it can get you into sticky situations easily if you dont know what you're doing
The design for this truck was based on the maverick strada XT, as well as a variety of different high end nitro truggies

Anyways, enough reading, time to drive! again, thank you for downloading this car, and I hope you have fun with it.


tools used: Blender, Gimp, and RVGL

You have my permission to do whatever you wish with this car, as long as they abide by the following rules
1. credit all apropriate authors if you re-release
2. you may not use this work for commercial purposes