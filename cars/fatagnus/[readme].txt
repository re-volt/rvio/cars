Car information
================================================================
Car name: Fat Agnus
Car Type: Remodel
Top speed: 62 kph
Rating/Class: 3 (Semi-Pro)
Installed folder: ...\cars\fatagnus
Description: 

Fat Agnus is a remodel of Mighty's Crankenwagon.

The car has completly new params made by Skarma! Bonzcy!

Fun fact: Back in 1998, the worm name generator in Worms 2
named one of my worms "Fat Agnus". Since this, I used this
name for several things now and then. The time has come to
name a Re-Volt car like this. ;)

Have fun!
-Kiwi

Author Information
================================================================
Base Model: MightyCucumber
Parameters: Skarma
Texture: Kiwi

 
Construction
================================================================
Editor(s) used: Notepad++, PhotoImpact12
 

Copyright / Permissions
================================================================
Please do not change any parts of this car without asking, as long it's not for your personal use.
You can reuse any parts of this car for your own car, as long you mention the
original creators accordingly.


Version 1.1 from March 2nd, 2020