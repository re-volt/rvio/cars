Release Number: 1    Car Name: IVan  Date: 10-10-2001
================================================================
Car name                : IPico   
Install in folder       : Re-volt main folder

Modi Author for Re-Volt : Converted by BurnRubr  
Tuning of Parameters by : DSL_Tile.Thanks mate.    
Email Address           : burnrubr@email.com
Web Site                : www.rch.d2g.com\burnrubr\

Description             : 4x4 Offroad truck
Car Base                : Converted from Insane Racing by Codemasters
RATING                  : PRO car\ 40mph

================================================================
Known Bugs              : None Known.

* Copyright / Permissions *

Authors MAY REPAINT AND RETUNE THIS CAR.
Providing CREDIT is given to the following:
Insane Racing by Codemasters for car mesh.
BurnRubr for the conversion of the car to Re-Volt.

