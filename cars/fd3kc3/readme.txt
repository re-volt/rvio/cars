﻿Car information
================================================================
Car name                : KC-3
Car Type  		: Repaint
Top speed 		: 38 mph
Rating/Class   		: Semi-Pro
Installed folder       	: ...\cars\fd3kc3

Author Information
================================================================
Author Name 		: Xarc
Email Address           : warrockrockwar@hotmail.it
 
Construction
================================================================
Base           		: Jaguar (by Allan1)
Editor(s) used 		: MediBang Paint Pro
 
Additional Credits 
================================================================
Credits to Allan1 for the original car

Copyright / Permissions
================================================================
You may do whatever you want with this CAR.