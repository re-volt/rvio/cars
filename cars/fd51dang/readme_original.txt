Bruiser is a Monster Truck version of RiffRaff's Dr. Death pickup.
It is lifted, has Rotor tires, Harvester axles and a fexible whip antenna in the bed.
A lot of other parameters have changed, top speed is about 50mph.
The repaint took me a few tries to get it right. For example the left inside of the bed is
the same texture as the left outside. On the right however, inside and outside textures
are not the same.
The front and rear are copied (and resized) from a skin from the upcoming auto combat game
Interstate '82 (again). The rest is painted in matching green and I had to redo the 
windows 3 times because they look a bit misaligned on the original car.

Unzip in the re-volt\cars folder.


Ruud Mussen   r.mussen@wxs.nl    ICQ:  Darkwing    23675002





> This is a custom which took me several tries to get looking right. Its made from 23
> different blocks, pieced together.
> 
> Dr. Death is just a sporty pickup with H.R.Giger graphics. Thought the name would be
> fitting, since this car is set up for Tag play, sorta. Its pretty roadworthy too, in
> case the host races instead of Tag.
>
> The tools used to make this car are
> 1) 3dsmax R2.5 - To make the car body,  and wheels-tires.
> 2) Adobe Photoshop 4.1 - To paint the skin.
> 3) RHQ Car Manager - To make this compilation a little easier.
> 
> Have fun!  
> 
> RiffRaff
