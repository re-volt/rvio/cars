Car information
================================================================
Car name                : Rolls Royce Phantom
Car Type  		: Original
Top speed 		: ? mph/kph
Rating/Class   		: pro
Install folder       	: ...\cars\rrphantom
Description             :Rolls Royce Phantom, sooo smooth it irons the bumps in the road for you 

 
Author Information
================================================================
Author Name 		: MOH
Email Address           : carmadman@hotmail.com	
Misc. Author Info       : 
 
Construction
================================================================
Base           		: original
Poly Count     		: ? polies for the body
               		: ? polies for each wheel
Editor(s) used 		:  photoshop 7,zmodeler,rvshade,rvsizer,rvdblsd,rvtrans
 
Additional Credits 
================================================================
Everyone whos still plays revolt for keeping the game alive
Juicemane for suggesting it

 
Copyright / Permissions
================================================================
Authors MAY use this Car as a base to build additional cars.  
You MAY distribute this CAR, provided you include this file, with no 
modifications.  You may distribute this file in any electronic format 
(BBS, Diskette, CD, etc) as long as you include this file intact.

 
Where else to get this CAR
================================================================
FTP sites		:
Website  		:
Other			:
