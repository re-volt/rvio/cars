Car information
================================================================
Car name                : Harmor
Car Type  		: Original
Top speed 		: 54 kph (45 kph Average)
Rating/Class   		: 1 (amateur)
Installed folder       	: ...\cars\harmrcss
Description             : the supercar-offroad concept

Harmor is a prototype concept car hybrid of a supercar and an offroader.
the car features unparalleled terrain stability befitting of its rugged-yet-streamlined design.

Harmor's floaty handling allows it to maintain control on any surface grip. along with its soft suspension and offroad tuning, Harmor stays stable even on the most bumpy terrains.
not only that, Harmor's handling is versatile enough to take corners with ease, having a tight cornering radius. its moderate mass means the car will stay planted on the road.

despite being stable, that doesnt mean the car is easy to control.
the car's floaty handling can be finicky and inconsistent.
this floaty handling results in Harmor's pace being inconsistent and unpredictable, sometimes it is faster and sometimes it is slower.

being a prototype concept car, Harmor comes with effective strengths that is bogged down by its unoptimized repertoire. winning with this car requires taking advantages of terrain shortcuts that no other car can take.




uthor Information
================================================================
Author Name 		: shara coronvi
Email Address           : sharacolonvee@protonmail.com
Other Info		: pretty much a recent Re-Volt veteran. 
                          a good racer and drifter. experienced in parametering and repaints.
                          hoster of unique fresh unusual online sessions. also a drawing "artist" too.
 
Construction
================================================================
Base           		: Original
Editor(s) used 		: Notepad, Paint.NET, SketchUp and Blender
 
Additional Credits 
================================================================
Thanks and Credits to those who inspired or helped.

Acclaim for this great game
Microsoft for Notepad
Paint.NET developers for the afromentioned program
Marv and Huki for the Blender plugin
uses wheel rim texture made by BloodBTF
3rd skin is based on "Ryback Tornado" from Split/Second Velocity



Copyright / Permissions
================================================================
If you're considering to put this car in a pack, you may NOT modify the car's handling.
if the car handling doesnt fit into the theme of the pack (ie too slow, too fast), simply dont put this car in.
though, you are ALLOWED to make a NEW car using this as a base, and then put that new car into a pack.

Due to the new skin selection system, standalone repaints of this car is not allowed (repaints resulting in a seperate car with a different foldername), even if it has modified params.
Above rule is to prevent this original creation from being overshadowed by any new potential repaints of this car.
Repaints of this car can only be released as skins for this car's specific foldername, ask the author if you want your skin to be published and bundled with the car.
(now with new Re-Volt World website, people are allowed to make and release skins for this car's foldername, provided that the skins are not made as a new seperate car)

Remodels and Originals based on this car are allowed without further limits.
Remapping the UV map without changing the body geometry for repainting doesnt count as Remodel, therefore not allowed.
Other than those said above, you may do whatever you want with this car.
