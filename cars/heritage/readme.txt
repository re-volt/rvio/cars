Information
-----------------
Car Name:	Heritage
Rating:		Semi-Pro
Top speed:	35.3 mph
Acceleration:	3.15 m/s�
Weight:		1.7 kg
Author:		Saffron


Lore
-----------------
The Toyama Heritage is a much-revered sportscar, emerging as the flagship for the Japanese manufacturer in the late 90s. Toy-Volt made quick haste towards securing a deal with them, ensuring them the rights to create high-quality R/C cars based off of it.
This is the sixth-generation model, with the livery based off one of of their real-life racing counterparts. Toy-Volt fine-tuned the performance of the Heritage R/C car to boast speed and cornering for those interested in taking the car to the track on a more semi-professional level.
Unlike the previous generations however, this one sports experimental four-wheel steering technology, which sparked a lot of controversy by enthusiast circles. But as those open-minded give themselves time to learn the nuances of the sixth-generation Toyama Heritage R/C car, one will quickly understand what the name alludes to.
Much like the real-life variant, it builds upon the past what made the car popular in the first place, its heritage, and ensures it's ready for what the future brings forth, be it enthusiasm in the corners, or hardship along the way.


Requirements
-----------------
You MUST use the latest RVGL patch for the textures to display properly.


Credits
-----------------
The Blender Foundation for Blender
Marv, Martin and Huki for the Blender plug-in
Jigebren for PRM2HUL
TTDriver for the wheel texture
The RVGL Team for RVGL
Rick Brewster for Paint.NET


Permission
-----------------
My creations cannot be distributed in any content pack associated with RVON (Re-Volt Online).
User created skins for my cars cannot be added to content packs without my permission.
If you want to make modifications to a track of mine for special events and tournaments, do it under a different name and folder name, alongside specifying said modifications in a separate Readme file.
If you want to convert a creation of mine to another game, make sure to credit me.
If you make a car derived from this one (a repaint or a remodel), the permissions for that car must match this one's permissions.