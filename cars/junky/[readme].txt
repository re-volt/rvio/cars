
  J U N K Y
  
  Version 1.1 from May 14th, 2021

================================================================
  Car information
================================================================

Date      : 14/05/2021
Car Name  : Junky
Author    : Kiwi
Car Type  : Remodel
Folder    : ...\cars\junky
Top Speed : 31 mph
Mass      : 2,8 KG
Rating    : 0 (Rookie)

Junky is a remodel of Allan1's awesome Dump Truck. It was one of
my favourite cars since I rediscovered the game in 2017. In 2018
I made an alternate skin for Dump Truck, called "AMA Dump Co.".
Now I decided to make a standalone car out of this skin.

I (very) slightly adjusted the model. The wheels are smaller, as
an example. The texture has more shading and more details
compared to my "AMA Dump Co." one. The params are nearly identic
to Dump Truck, with some very small adjustments.

Junky is gas powered, as a difference to Dump Truck. Also, it has
a custom honk sound, which I borrowed from the 1997 game Ignition,
made made UDS.

Have fun with Junky!
- Kiwi


================================================================
  Construction
================================================================

Polygons       : 860 (Body, Axles, Wheels)

Base           : Dump Truck by Allan1
                 AMA Dump Co. by Kiwi

Editors used   : Blender 2.79b
                 Ulead Photo Impact 12
                 Audacity
                 Notepad++


================================================================
  Thank You
================================================================

+ Allan1 for Dump Truck
+ UDS for the honk sound


================================================================
  Copyright / Permissions
================================================================

Please do not change any parts of this car without asking, as
long it's not for your personal use. You can reuse any parts of
this car for your own car, as long you mention the original
creators accordingly.
