made by BloodBTF, April 2021

Car: RC Winglet
Class: Electric
Rating: Pro
Trans: RWD
Top Speed: 42MPH
Accel: great
Handling: good
Mass: 1.4kg

A lively off road racer with a lot of attitude to its name. Top speed and acceleration are the main strengths to this buggy, but its rear weight bias significantly reduces its maneuvarability. One of its quirks is exceptional stability when jumping, flying and landing nice and level regardless of any reason for you to go airborne. Wings to the sky! 

tools used: RVGL, Blender, GIMP

Fun fact: this car was inspired by the ECX boost RC buggy, with the livery being inspired by the Tamiya Desert Gator

you have my permission to do whatever you want with this car, as long as it abides by the following rules:
1. you must credit all appropriate authors
2. you may not use this work for commercial purposes