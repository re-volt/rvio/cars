made by BloodBTF, December 2020

Car: Spectron
Class: Special 
Rating: Super-Pro
Trans: FWD
Top Speed: 46MPH
Accel: poor
Handling: great
Mass: 1.8kg

After being kept behind closed doors for many years under tight research and development, the ultimate RC car was complete. Using a super capacitor of unknown origin as it's powerhouse, Spectron is one of the fastest cars on the track. This comes at a price though, which is acceleration. From a dead stop, this car takes forever to get up to speed. But when you get there, you can easily overtake any car in your way. The handling department wasn't overlooked either, as it's four wheel steering can get you around any tight corner with minimal effort.

Anyway, I hope you enjoy this car, and thanks for downloading!

Special thanks to Gotolei and MightyCucumber for the wheel texture(s), and Acclaim for the engine sound

tools used: RVGL, Blender, GIMP

you have my permission to do whatever you want with this car, as long as it abides by the following rules:
1. you must credit all appropriate authors
2. you may not use this work for commercial purposes